import React, {Component} from 'react';
import {
  AppRegistry,
  ScrollView,
  StyleSheet,
  Text,
  View
} from 'react-native';

// Dependencias
import dgram from 'react-native-udp';                     // https://www.npmjs.com/package/react-native-udp
import AudioTrack from 'react-native-audio-track';        // https://www.npmjs.com/package/react-native-audio-track


// TODO: criar modulo
var stream_counter = 0;
var stream_last_counter = 0;
var stream_pcm;
var stream_previous_value = 0;
var stream_addr;
var stream_port;
var stream_socket;

function stream_lowpass(samples, cutoff, sampleRate, numChannels) {
  let rc = 1.0 / (cutoff * 2 * Math.PI);
  let dt = 1.0 / sampleRate;
  let alpha = dt / (rc + dt);
  let last_val = [];
  let offset;
  for (let i = 0; i < numChannels; i++) {
    last_val[i] = samples[i];
  }
  for (let i = 0; i < samples.length; i++) {
    for (let j = 0; j < numChannels; j++) {
      offset = (i * numChannels) + j;
      last_val[j] =
        last_val[j] + (alpha * (samples[offset] - last_val[j]));
      samples[offset] = last_val[j];
    }
  }
}
function stream_init(addr, port){
  stream_addr = addr;
  stream_port = port;

  stream_socket = dgram.createSocket({
    type: 'udp4'
  });
  stream_socket.bind(5005, function(err) {
    if (err) throw err;
  })
  // Faz o dispositivo sair da tela de QRCode e passa a enviar os dados
  let data = "start";
  stream_socket.send(data, 0, data.length, stream_port, stream_addr, function(err) {
  });

  stream_socket.on('message', function(data, rinfo) {
    stream_write(data, rinfo);
  })

  AudioTrack.init({
    bufferSize: 7680*10,
    sampleRate: 7680,
    bitsPerChannel: 16,
    channelsPerFrame: 1,
  })

}
function stream_play(){
  AudioTrack.play();
}
function stream_pause(){
  AudioTrack.pause();
}
function stream_set_volume(vol){
  AudioTrack.setVolume(vol);
}     // float 0..1
function stream_write(data, rinfo){
  if(rinfo["address"] == stream_addr){
    stream_counter = data[0] + (data[1] << 8);
    if(stream_counter == 0) stream_last_counter = stream_counter;

    if((stream_counter!=0) && stream_last_counter < stream_counter) {
      let i,j;

      stream_pcm = data.slice(2, data.length);

      const audio = [];
      for (i = 0; i < stream_pcm.length / 2; i++) {
        audio[i] = (stream_pcm[i * 2] | stream_pcm[(i * 2) + 1] << 8);
        if ((audio[i] & 0x8000) > 0) {
          audio[i] = audio[i] - 0x10000;
        }
      }
      var resample = [];
      let diff = stream_previous_value - audio[0];
      for (j = 0; j < 15; j++) {
        resample.push(stream_previous_value - (j * (diff) / 15));
      }
      for (i = 0; i < audio.length - 1; i++) {
        let diff = audio[i] - audio[i + 1];
        for (j = 0; j < 15; j++) {
          resample.push(audio[i] - (j * (diff) / 15));
        }
      }
      stream_previous_value = audio[audio.length - 1];
      stream_lowpass(resample, 30, 7680, 1);
      AudioTrack.write(resample, 0, resample.length);
    }
  }
}

export default class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    // Pegar IP pelo QRCode
    stream_init('192.168.0.146', 5005);
    stream_set_volume(1);
    stream_play();
  }


  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

